import socket
import struct
import sys
"""
from tkinter import *
import tkinter.messagebox

tk = Tk()
tk.title("Tic Tac Toe - Servidor")
"""
multicast_addr = '224.0.0.1'
port = 3000
port_listen = 3010

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
TCP_IP = s.getsockname()[0]
TCP_PORT = 5005
BUFFER_SIZE = 1024
s.close()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

tablero = [[0,0,0],[0,0,0],[0,0,0]]

def enviar():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    ttl = struct.pack('b', 1)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
    message = "Welcome to the Jungle Baby"
    sock.sendto(message.encode(), (multicast_addr, port))
    sock.close()

def iniciarTablero():
    print("\n\n****** TIC TAC TOE ******\n\n")
    print("Jugador 1: ", jugador)
    print("Jugador 2: ", jugador_cliente)
    print("\n\n *** TABLERO DE COORDENADAS *** \n\n-Use las siguientes coordenadas para indicar la posición que desea marcar.\n-Para salir ingrese quit().")
    for i in range(3):
        if i != 0:
            print("\n----- ----- -----")
        else:
            print("\n")
        for j in range(3):
            tablero[i][j] = 0
            if j != 2:
                print(" {0},{1} |".format(i,j), end="")
            else:                 
                if i != 2:
                    print(" {0},{1} ".format(i,j), end="")
                else:
                    print(" {0},{1} ".format(i,j))
    print("\n ** Inicia el Jugador 1: {0} ** \n".format(jugador))

def pintarTablero(x,y,njugador):
    tablero[int(x)][int(y)] = int(njugador)   
    if njugador==1:
        nombre = jugador
    else:
        nombre = jugador_cliente

    print("\n Jugada del jugador {0}: {1}".format(njugador,nombre)) 
    for i in range(3):
        if i != 0:
            print("\n--- --- ---")
        else:
            print("\n")
        for j in range(3):
            if tablero[i][j] == 1:
                ficha = 'X'
            elif tablero[i][j] == 2:
                ficha = 'O'
            else:
                ficha = ' '

            if j != 2:
                print(" {0} |".format(ficha), end="")
            else:                 
                if i != 2:
                    print(" {0} ".format(ficha), end="")
                else:
                    print(" {0} ".format(ficha))

def casillaLibre(x,y):
    fin = False
    if int(x) > 2 or int(x) < 0 or int(y) > 2 or int(y) < 0 :
        print("*** JUGADA INVALIDA: las coordenadas se encuentran fuera de rango ***")
        message = input(str("\n{0}, ingrese las coordenadas a jugar (Ejemplo: 0,1 ): ".format(jugador)))
        pintarTablero(message[0],message[2],1)
    else:
        if tablero[int(x)][int(y)] != 0 :        
            print("*** JUGADA INVALIDA: La casilla está ocupada ***")
            message = input(str("\n{0}, ingrese las coordenadas a jugar (Ejemplo: 0,1 ): ".format(jugador)))
            pintarTablero(message[0],message[2],1)
        else:
            pintarTablero(x,y,1)
            message = x+","+y    
    ganador = validarGanador()
    if ganador != 0:
        print("\n\n GANADOR: Jugador {0}\n\n".format(ganador))        
        message = message + "GANADOR: Jugador {0}".format(ganador)
        conn.send(message.encode())
        fin = True
    conn.send(message.encode())
    return fin

def validarGanador():
    ganador = 0

    for i in range(3):
        if (tablero[i][0] != 0) and (tablero[i][0] == tablero[i][1] and tablero[i][0] == tablero[i][2]):
            ganador = tablero[i][0]
            break
    
    for i in range(3):
        if (tablero[0][i] != 0) and (tablero[0][i] == tablero[1][i] and tablero[0][i] == tablero[2][i]):
            ganador = tablero[0][i]
            break
    
    if (tablero[0][0] != 0) and (tablero[0][0] == tablero[1][1] and tablero[0][0] == tablero[2][2]):
            ganador = tablero[0][0]

    if (tablero[0][2] != 0) and (tablero[0][2] == tablero[1][1] and tablero[0][2] == tablero[2][0]):
            ganador = tablero[0][2]
    
    return ganador

enviar()
s.bind((TCP_IP, TCP_PORT))
s.listen()
conn, addr = s.accept()
print ('\nSe ha conectado el cliente: {0}, Bienvenido!\n'.format(addr[0]))
data = conn.recv(BUFFER_SIZE)
print ("Mensaje Recibido:", data.decode())

jugador = input(str("Nombre del Jugador 1: "))
conn.send(jugador.encode()) 

message = conn.recv(1024)
jugador_cliente = message.decode()
print("Bienvenido ", jugador_cliente)

iniciarTablero()
while True:
    message = input(str("\n{0}, ingrese las coordenadas a jugar (Ejemplo: 0,1 ): ".format(jugador)))
    if "quit" in  message:
        message = "Se ha abandonado la conexion"
        conn.send(message.encode())
        print("\n")
        break
    fin = casillaLibre(message[0],message[2]) 
    if fin:
        break   
    message = conn.recv(1024)
    message = message.decode()
    if "abandonado" in message:
        print(message) 
        break       
    elif "GANADOR" in  message:
        pintarTablero(message[0],message[2],2)
        print("\n\n Ganador: Jugador 2\n\n")  
        break
    else:
        pintarTablero(message[0],message[2],2)
#tk.mainloop()