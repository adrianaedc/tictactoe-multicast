# Comunicaciones II
## Tic Tac Toe 

Juego desarrollado en Python, es ejecutado en consola y permite jugar Tic Tac Toe entre dos computadoras, haciendo uso de una comunicación multicast que posteriormente es convertida a TCP/IP.

## Desarrolladores
* [Adriana Delgado (@adrianaedc)](https://gitlab.com/adrianaedc)
* [Karen Chacón]