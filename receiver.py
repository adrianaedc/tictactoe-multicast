import socket
import struct
import sys
"""
from tkinter import *
import tkinter.messagebox

tk = Tk()
tk.title("Tic Tac Toe - Cliente")
"""
multicast_addr = '224.0.0.1'
port = 3000
port_listen = 3010

TCP_IP = ''
TCP_PORT = 5005
BUFFER_SIZE = 1024

tablero = [[0,0,0],[0,0,0],[0,0,0]]

def iniciarTablero():
    print("\n\n****** TIC TAC TOE ******\n\n")
    print("Jugador 1: ", jugador_server)
    print("Jugador 2: ", jugador)
    print("\n\n *** TABLERO DE COORDENADAS *** \n\n-Use las siguientes coordenadas para indicar la posición que desea marcar.\n-Para salir ingrese quit().")
    for i in range(3):
        if i != 0:
            print("\n----- ----- -----")
        else:
            print("\n")
        for j in range(3):
            tablero[i][j] = 0
            if j != 2:
                print(" {0},{1} |".format(i,j), end="")
            else:                 
                if i != 2:
                    print(" {0},{1} ".format(i,j), end="")
                else:
                    print(" {0},{1} ".format(i,j))
    print("\n ** Inicia el Jugador 1: {0} ** \n".format(jugador_server))

def pintarTablero(x,y,njugador):
    tablero[int(x)][int(y)] = int(njugador)
    if njugador==1:
        nombre = jugador_server
    else:
        nombre = jugador

    print("\n Jugada del jugador {0}: {1}".format(njugador,nombre))
    for i in range(3):
        if i != 0:
            print("\n--- --- ---")
        else:
            print("\n")
        for j in range(3):
            if tablero[i][j] == 1:
                ficha = 'X'
            elif tablero[i][j] == 2:
                ficha = 'O'
            else:
                ficha = ' '

            if j != 2:
                print(" {0} |".format(ficha), end="")
            else:                 
                if i != 2:
                    print(" {0} ".format(ficha), end="")
                else:
                    print(" {0} ".format(ficha))

def casillaLibre(x,y):
    fin = False
    if int(x) > 2 or int(x) < 0 or int(y) > 2 or int(y) < 0 :
        print("*** JUGADA INVALIDA: las coordenadas se encuentran fuera de rango ***")
        message = input(str("\n{0}, ingrese las coordenadas a jugar (Ejemplo: 0,1 ): ".format(jugador)))
        pintarTablero(message[0],message[2],2)
    else:
        if tablero[int(x)][int(y)] != 0 :        
            print("*** JUGADA INVALIDA: La casilla está ocupada ***")
            message = input(str("\n{0}, ingrese las coordenadas a jugar (Ejemplo: 0,1 ): ".format(jugador)))
            pintarTablero(message[0],message[2],2)
        else:
            pintarTablero(x,y,2)
            message = x+","+y  
    ganador = validarGanador()
    if ganador != 0:
        print("\n\n GANADOR: Jugador {0}\n\n".format(ganador))        
        message = message + "GANADOR: Jugador {0}".format(ganador)
        s.send(message.encode())
        fin = True
    s.send(message.encode())
    return fin

def validarGanador():
    ganador = 0

    for i in range(3):
        if (tablero[i][0] != 0) and (tablero[i][0] == tablero[i][1] and tablero[i][0] == tablero[i][2]):
            ganador = tablero[i][0]
            break
    
    for i in range(3):
        if (tablero[0][i] != 0) and (tablero[0][i] == tablero[1][i] and tablero[0][i] == tablero[2][i]):
            ganador = tablero[0][i]
            break
    
    if (tablero[0][0] != 0) and (tablero[0][0] == tablero[1][1] and tablero[0][0] == tablero[2][2]):
            ganador = tablero[0][0]

    if (tablero[0][2] != 0) and (tablero[0][2] == tablero[1][1] and tablero[0][2] == tablero[2][0]):
            ganador = tablero[0][2]
    
    return ganador

s = socket.socket()
bind_addr = '0.0.0.0'
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
membership = socket.inet_aton(multicast_addr) + socket.inet_aton(bind_addr)

sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, membership)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

sock.bind((bind_addr, port))
print("*** Escuchando servidor ***")
message, address = sock.recvfrom(255)

TCP_IP = str(address[0])
print('-Conectado a servidor con ip: {0} \n-Mensaje del Servidor: {1}'.format(TCP_IP,message.decode()))

sock.close()
MESSAGE = "Hola Servidor {0}! Soy un Cliente conectado por TCP".format(TCP_IP)

s.connect((TCP_IP, TCP_PORT))
s.send(MESSAGE.encode())

message = s.recv(1024)
jugador_server = message.decode()
print("Bienvenido ", jugador_server)

jugador = input(str("Nombre del Jugador 2: "))
s.send(jugador.encode())

iniciarTablero()
while True:
    message = s.recv(1024)
    message = message.decode()
    if "abandonado" in message:
        print(message)
        break          
    elif "GANADOR" in  message:
        pintarTablero(message[0],message[2],1)
        print("\n\n GANADOR: Jugador 1 \n\n")  
        break
    else:
        pintarTablero(message[0],message[2],1)
    message = input(str("\n{0}, ingrese las coordenadas a jugar (Ejemplo: 0,1 ): ".format(jugador)))
    if message == "quit()":
        message = "Se ha abandonado la conexion"
        s.send(message.encode())
        print("\n")
        break    
    
    fin = casillaLibre(message[0],message[2]) 
    if fin:
        break
#tk.mainloop()